require 'test_helper'
require 'rav/codec'

describe RAV::Codec do
  let(:codec) { RAV::Codec.first }
  let(:encoder) { RAV::Codec.find(&:encoder?) }
  let(:decoder) { RAV::Codec.find(&:decoder?) }

  it 'acts like an enumerable' do
    RAV::Codec.each { |c| assert_instance_of RAV::Codec, c }
  end

  it 'has a table of codecs' do
    RAV::Codec.table.each do |name, id|
      assert_includes RAV::Codec.ids, id
      assert_includes RAV::Codec.names, name.to_s.downcase
    end
  end

  it 'knows about encoders and decoders' do
    RAV::Codec.encoders.each { |c| assert c.encoder? }
    RAV::Codec.decoders.each { |c| assert c.decoder? }
  end

  it 'can get a context' do
    assert_equal codec, codec.context.codec
  end
end
