require "test_helper"

describe RAV do
  it 'is a module' do
    assert_kind_of Module, RAV
  end

  describe 'encoder!' do
    it 'can find an encoder' do
      assert_equal 'mpeg2video', RAV.encoder!(:MPEG2VIDEO).name
    end

    it 'complains if it cannot find an encoder' do
      assert_raises(RAV::CodecNotFound) { RAV.encoder!(:NO_SUCH_ENCODER) }
    end
  end

  describe 'decoder!' do
    it 'can find a decoder' do
      assert_equal 'pcm_s16le', RAV.decoder!(:PCM_S16LE).name
    end

    it 'complains if it cannot find a decoder' do
      assert_raises(RAV::CodecNotFound) { RAV.decoder!(:NO_SUCH_DECODER) }
    end
  end
end
