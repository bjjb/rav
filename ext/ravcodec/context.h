/*
 * Initializes the Context class under the given module.
 */
void context_init(VALUE);

/*
 * Gets a new Context, opened from the given AVCodec.
 */
VALUE context_get(AVCodec *);

/*
 * Gets the AVCodecContext from the object
 */
const AVCodecContext *context_get_avcontext(VALUE);
