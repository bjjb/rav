#include <ruby.h>
#include <libavcodec/avcodec.h>
#include "context.h"

static VALUE cCodec = Qnil;
static VALUE aCodecs = Qnil;

static size_t codec_size(const void *ptr) {
  return sizeof(AVCodec);
}

static const rb_data_type_t tCodec = {
  "Codec",
  { 0, 0, codec_size },
  0, 0, RUBY_TYPED_FREE_IMMEDIATELY
};

/* Gets the codec's underlying AVCodec */
const AVCodec *codec_get_avcodec(VALUE oCodec) {
  AVCodec *c;
  TypedData_Get_Struct(oCodec, AVCodec, &tCodec, c);
  return c;
}

static VALUE name(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return rb_str_new2(c->name);
}

static VALUE long_name(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return rb_str_new2(c->long_name);
}

static VALUE media_type(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return INT2FIX(c->type);
}

static VALUE codec_id(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return INT2FIX(c->id);
}

static VALUE capabilities(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return INT2FIX(c->capabilities);
}

static VALUE channel_layouts(VALUE oCodec) {
  VALUE ary = rb_ary_new();
  AVCodec const *c = codec_get_avcodec(oCodec);
  for (uint64_t const *ch = c->channel_layouts; ch; ch++) {
    rb_ary_push(ary, INT2NUM(*ch));
  }
  return ary;
}

static VALUE samplerates(VALUE oCodec) {
  VALUE ary = rb_ary_new();
  AVCodec const *c = codec_get_avcodec(oCodec);
  for (int const *ss = c->supported_samplerates; ss; ss++) {
    rb_ary_push(ary, INT2FIX(*ss));
  }
  return ary;
}

static VALUE sample_formats(VALUE oCodec) {
  VALUE ary = rb_ary_new();
  AVCodec const *c = codec_get_avcodec(oCodec);
  for (int const *sf = c->sample_fmts; sf; sf++) {
    rb_ary_push(ary, INT2FIX(*sf));
  }
  return ary;
}

static VALUE framerates(VALUE oCodec) {
  VALUE ary = rb_ary_new();
  AVCodec const *c = codec_get_avcodec(oCodec);
  for (AVRational const *fr = c->supported_framerates; fr; fr++) {
    if (!(fr->num && fr->den)) break;
    char *s = calloc(40, sizeof(char));
    snprintf(s, sizeof(s), "%d/%dr", fr->num, fr->den);
    rb_ary_push(ary, rb_eval_string(s));
    free(s);
  }
  return ary;
}

static VALUE profiles(VALUE oCodec) {
  VALUE profiles = rb_funcall(rb_cHash, rb_intern("new"), 0);
  AVCodec const *c = codec_get_avcodec(oCodec);
  for (AVProfile const *p = c->profiles; p; p++) {
    if (p->profile == FF_PROFILE_UNKNOWN) break;
    rb_funcall(profiles, rb_intern("store"), 2,
               INT2FIX(p->profile), rb_sprintf("%s", p->name));
  }
  return profiles;
}

static VALUE is_encoder(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return av_codec_is_encoder(c) ? Qtrue : Qfalse;
}

static VALUE is_decoder(VALUE oCodec) {
  AVCodec const *c = codec_get_avcodec(oCodec);
  return av_codec_is_decoder(c) ? Qtrue : Qfalse;
}

static VALUE context(VALUE oCodec) {
  ID ivarCodec = rb_intern("@codec");
  ID ivarContext = rb_intern("@context");
  VALUE oContext = rb_ivar_get(oCodec, ivarContext);
  if (oContext == Qnil) {
    AVCodec const *c = codec_get_avcodec(oCodec);
    oContext = context_get(c);
    rb_ivar_set(oContext, ivarCodec, oCodec);
    rb_ivar_set(oCodec, ivarContext, oContext);
  }
  return oContext;
}

void add(AVCodec *c) {
  VALUE codec = TypedData_Wrap_Struct(cCodec, &tCodec, c);
  rb_ary_push(aCodecs, codec);
}

/**
 * Defines an AVCodec wrapper class (called Codec) under the given module.
 * Also registers all AVCodecs.
 */
void codec_init(VALUE mod) {
  aCodecs = rb_ary_new();
  cCodec = rb_define_class_under(mod, "Codec", rb_cData);

  context_init(cCodec);

  avcodec_register_all();
  for (AVCodec *c = av_codec_next(NULL); c; c = av_codec_next(c)) add(c);

  rb_extend_object(cCodec, rb_mEnumerable);

  rb_define_method(cCodec, "context", context, 0);
  rb_define_method(cCodec, "codec_id", codec_id, 0);
  rb_define_method(cCodec, "name", name, 0);
  rb_define_method(cCodec, "long_name", long_name, 0);
  rb_define_method(cCodec, "media_type", media_type, 0);
  rb_define_method(cCodec, "capabilities", capabilities, 0);
  rb_define_method(cCodec, "channel_layouts", channel_layouts, 0);
  rb_define_method(cCodec, "samplerates", samplerates, 0);
  rb_define_method(cCodec, "sample_formats", sample_formats, 0);
  rb_define_method(cCodec, "framerates", framerates, 0);
  rb_define_method(cCodec, "profiles", profiles, 0);
  rb_define_method(cCodec, "encoder?", is_encoder, 0);
  rb_define_method(cCodec, "decoder?", is_decoder, 0);
  rb_define_alias(cCodec, "id", "codec_id");
  rb_define_alias(cCodec, "type", "media_type");
  rb_define_const(cCodec, "VERSION", rb_sprintf("%d.%d.%d",
                                                LIBAVCODEC_VERSION_MAJOR,
                                                LIBAVCODEC_VERSION_MINOR,
                                                LIBAVCODEC_VERSION_MICRO));
  rb_define_const(cCodec, "AVCODECS", aCodecs);
}
