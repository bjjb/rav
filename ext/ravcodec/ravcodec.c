#include <ruby.h>
#include <libavcodec/avcodec.h>
#include "codec.h"

static VALUE mRAV = Qnil;

/* Initializes the extension */
void Init_ravcodec(void) {
  if (mRAV != Qnil) return;
  mRAV = rb_define_module("RAV");
  codec_init(mRAV);
}
