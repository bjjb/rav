#include <ruby.h>
#include <libavcodec/avcodec.h>
#include "codec.h"

static VALUE cCodec = Qnil;
static VALUE cContext = Qnil;

/* Returns the size of an AVCodecContext */
static size_t context_size(const void *ptr) {
  return sizeof(AVCodecContext);
}

/* Frees an avcontext descriptor */
static void context_free(void *ptr) {
  AVCodecContext *ctx = ptr;
  avcodec_free_context(&ctx);
}

/* Describes an AVCodecContext for Ruby */
static const rb_data_type_t tContext = {
  "AVCodecContext",
  { 0, context_free, context_size },
  0, 0, RUBY_TYPED_FREE_IMMEDIATELY
};

VALUE codec(VALUE oContext) {
  return rb_ivar_get(oContext, rb_intern("@codec"));
}

/* Gets a context for a codec */
VALUE context_get(AVCodec const *c) {
  AVCodecContext *ctx = avcodec_alloc_context3(c);
  return ctx ? TypedData_Wrap_Struct(cContext, &tContext, ctx) : Qnil;
}

/* Gets the context's underlying AVCodecContext */
const AVCodecContext *context_get_avcontext(VALUE oContext) {
  AVCodecContext *ctx;
  TypedData_Get_Struct(oContext, AVCodecContext, &tContext, ctx);
  return ctx;
}

/* Defines the Context class under the given module. */
void context_init(VALUE super) {
  cCodec = super;
  cContext = rb_define_class_under(super, "Context", rb_cData);
  rb_define_method(cContext, "codec", codec, 0);
}
