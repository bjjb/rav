/**
 * Initializes the Codec class under the given module.
 */
void codec_init(VALUE mod);

/**
 * Gets the underlying AVcontext from a struct.
 */
const AVCodec *codec_get_avcodec(VALUE);
