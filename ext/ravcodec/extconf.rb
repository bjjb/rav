require 'mkmf'
dir_config 'ravcodec'
abort unless have_library('avcodec')
create_makefile 'ravcodec'
