librav
======

These are (fairly) thin, low-level wrappers around the [FFMpeg][] libraries

- `ravcodec` wraps [libavcodec][] and provides `RAV::Codec`
- `ravdevice` wraps [libavdevice][] and provides `RAV::Device`
- `ravfilter` wraps [libavfilter][] and provides `RAV::Filter`
- `ravformat` wraps [libavformat][] and provides `RAV::Format`
- `ravutil` wraps [libavutil][] and provides `RAV::Util`
- `rpostproc` wraps [libpostproc][] and provides `RAV::PostProc`
- `rswresample` wraps [libswresample][] and provides `RAV::Resample`
- `rswscale` wraps [libswscale][] and provides `RAV::Scale`

They provide Ruby bindings to (some of) the structs and functions in these
libraries, and a more Ruby-esque interface.

[ffmpeg]: http://www.ffmpeg.org/documentation.html
[libavcodec]: http://www.ffmpeg.org/doxygen/3.2/group__libavc.html
[libavfilter]: http://www.ffmpeg.org/doxygen/3.2/group__lavfi.html
[libavformat]: http://www.ffmpeg.org/doxygen/3.2/group__libavf.html
[libavdevice]: http://www.ffmpeg.org/doxygen/3.2/group__lavd.html
[libavutil]: http://www.ffmpeg.org/doxygen/3.2/group__lavu.html
[libswresample]: http://www.ffmpeg.org/doxygen/3.2/group__lswr.html
[libpostproc]: http://www.ffmpeg.org/doxygen/3.2/group__lpp.html
[libswscale]: http://www.ffmpeg.org/doxygen/3.2/group__libsws.html
