lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "rav/version"

Gem::Specification.new do |s|
  s.name = "rav"
  s.version = RAV::VERSION
  s.authors = ["JJ Buckley"]
  s.email = ["jj@bjjb.org"]

  s.summary = %q{A Ruby wrapper around FFMpeg/libavcodec}
  s.description = <<-DESC
Provides Ruby bindings for libavcodec, and a fairly customisable command-line
interface for some common media conversion/inspection tasks.
  DESC
  s.homepage = "https://bjjb.gitlab.com/rav"
  s.license = "MIT"

  s.files = %w(LICENSE.txt README.md Rakefile Gemfile) +
            Dir["{bin,exe,lib}/**/*"] +
            Dir["ext/**/*.{h,c,rb}"]
  s.bindir = "exe"
  s.executables = s.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.extensions += s.files.grep(%r{ext/.*/extconf.rb})
  s.require_paths = ["lib"]

  s.add_development_dependency "bundler", "~> 1.16"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "minitest", "~> 5.0"
  s.add_development_dependency "rake-compiler"
end
