#!/usr/bin/env ruby
require 'rav'

# Examples of encoding and decoding. NOTE that these examples only deal with
# codecs (MPEG, MPEG-4, etc), not file-formats such as AVI or MKV - see the
# format examples for those.

# Demonstrates how to encode a video to h264.

def encode_video(filename, codec)
  puts 'Encode video file %s' % filename
  c = RAV.encoder!(codec)
  c.bit_rate! 400000
  c.width! 352
  c.height! 288
  c.time_base! 1/25r
  c.gop_size! 10
  c.max_b_frames! 1
  c.pix_fmt! :YUV420P
  c.priv_data!("preset", "slow") if c.id?(:H264)
  end
end

if __FILE__ == $0
  case ARGV.first
  when '-h', '--help', nil
    puts <<-HELP

Usage: #{__FILE__} TYPE

API example to decode/encode a media stream with rav.  Generates a synthetic
stream and encodes it to a file named test.h264, test.mp2 or test.mpg,
depending on TYPE.  The encoded stream is then decoded and written to a raw
data output. TYPE must be 'h264', 'mp2' or 'mpg'.
    HELP
    exit 1
  when 'h264'
    encode_video('test.h264', :H264)
  when 'mp2'
    encode_audio('test.mp2')
    decode_audio('test.pcm', 'test.mp2')
  when 'mpg'
    encode_video('test.mpg', :MPEG1VIDEO)
    decode_video('test%02d.pgm', 'test.mpg')
  else
    puts "Unknown type: #{ARGV[0]}. Try --help."
    exit 1
  end
end
