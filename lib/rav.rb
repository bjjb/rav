module RAV
  autoload :VERSION, 'rav/version'
  autoload :Codec, 'rav/codec'
  #autoload :Device, 'rav/device'
  #autoload :Filter, 'rav/filter'
  #autoload :Format, 'rav/format'
  #autoload :Util, 'rav/util'
  #autoload :PP, 'rav/pp'
  #autoload :Resample, 'rav/resample'
  #autoload :Scale, 'rav/scale'

  def codecs
    @codecs ||= to_a
  end

  def self.encoder(match)
    find_in_codecs(Codec.select(&:encoder?), match)
  end

  def self.encoder!(match)
    encoder(match) || raise(CodecNotFound, match)
  end

  def self.decoder(match)
    find_in_codecs(Codec.select(&:decoder?), match)
  end

  def self.decoder!(match)
    decoder(match) || raise(CodecNotFound, match)
  end

  private
  def self.find_in_codecs(codecs, match)
    case match
      when Symbol then codecs.find { |c| c.name.upcase == match.to_s.upcase }
      when Regexp then codecs.find { |c| c.name =~ match }
      when String then codecs.find { |c| c.name == match }
      else nil
    end
  end

  CodecNotFound = Class.new(Exception)
end
