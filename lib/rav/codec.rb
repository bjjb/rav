require 'ravcodec' # Load the C extension

module RAV
  class Codec
    def to_s
      '%s (%s)' % [ name, long_name ]
    end

    def inspect
      "<%s:%s/%s>" % [ self.class.name, id, name ]
    end

    def encode(&block)
      raise NotAnEncoder unless encoder?
    end

    class << self
      def each(&block)
        AVCODECS.each(&block)
      end

      def ids
        @ids ||= map { |c| c.id }
      end

      def names
        @names ||= map { |c| c.name }
      end

      def table
        @table ||= Hash[names.map(&:upcase).map(&:to_sym).zip(ids)]
      end

      def encoders
        @encoders ||= select(&:encoder?)
      end

      def decoders
        @decoders ||= select(&:decoder?)
      end
    end
  end
end
