# RAV

## Ruby Audio/Video

RAV is a Ruby wrapper around [libavcodec][], the library behind
[FFmpeg][ffmpeg], the world's best audio/video Swiss-army knife.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rav'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rav

## Usage

It ships with a command-line client; once installed, run

    $ rav help

to see a list of commands and options, or (if you have man(1)), run

    $ man rav

for the built-in manual.

To include it in your project, follow the Bundler procedure above, and check
the [docs][], either online or using `gem rdoc rav; gem server`.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then,
run `rake test` to run the tests. You can also run `bin/console` for an
interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`, and then
run `bundle exec rake release`, which will create a git tag for the version,
push git commits and tags, and push the `.gem` file to
[rubygems.org][rubygems].

## Contributing

Bug reports and pull requests are welcome on GitLab at
https://gitlab.com/bjjb/rav. This project is intended to be a safe, welcoming
space for collaboration, and contributors are expected to adhere to the
[Contributor Covenant][concov] code of conduct.

## License

The gem is available as open source under the terms of the [MIT
License][license].

## Code of Conduct

Everyone interacting in the Rav project’s codebases, issue trackers, chat
rooms and mailing lists is expected to follow the [code of conduct][coc].

[libavcodec]: https://en.wikipedia.org/wiki/Libavcodec
[ffmpeg]: https://ffmpeg.org
[coc]: https://gitlab.com/bjjb/rav/blob/master/CODE_OF_CONDUCT.md
[license]: https://opensource.org/licenses/MIT
[rubygems]: https://rubygems.org
[concov]: https://contributor-covenant.org
[docs]: https://bjjb.gitlab.com/rav/rdoc
